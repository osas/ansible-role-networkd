# Ansible role to manage systemd networkd

## Introduction

This role generate configuration for networkd. The configuration has
been made to be simple for most common needs.

You can then use the role like this:

    - role: networkd
      networks:
        eth0:
          cidr: 192.168.20.3/24
        eth2:
          # no IP defined, then using DHCP
        eth8:
          # configuration will be removed
          disabled: True
        virbr0:
          # this is a bridge
          bridged_ifaces:
            - eth1
          cidr: 192.168.42.3/24
          gateway: 192.168.42.1

## Variables:

- **networks**: hash of networks to configure
                the key is the name of the interface, the values are:
                  + cidr: the CIDR to configure the interface, DHCP is used if undefined
                  + gateway: optional default route
                  + bridged_ifaces: list of member interface of the bridge
                  + disabled: configuration is removed if True
- **admin_iface**: configure the provided interface using the ansible_host IP for
                   administration purpose

